# FoundryVTT lang de-DE

This module adds the option to select the German (GERMANY) language from the FoundryVTT settings menu. Selecting this option will translate various aspects of the program interface.

Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL
https://gitlab.com/Hydroxi/foundryvtt-lang-de-de/raw/master/de-DE/module.json
If this option does not work download the de-DE.zip file and extract its contents into the /resources/app/public/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.
